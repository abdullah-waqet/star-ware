import React from "react";
import { createBottomTabNavigator } from "react-navigation";

import PlanetsScreen from "../screens/PlanetsScreen";
import ProfileScreen from "../screens/ProfileScreen";
import Icon from "@expo/vector-icons/FontAwesome";

export default createBottomTabNavigator(
  {
    Planets: {
      screen: PlanetsScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ tintColor }) => (
          <Icon name="globe" color={tintColor} size={24} />
        )
      })
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: () => ({
        tabBarIcon: ({ tintColor }) => (
          <Icon name="user" color={tintColor} size={24} />
        )
      })
    }
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: "#F8F8F8",
      inactiveTintColor: "#586589",
      style: {
        backgroundColor: "#171F33"
      },
      tabStyle: {}
    }
  }
);
