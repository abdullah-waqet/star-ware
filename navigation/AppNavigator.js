import AppStack from "./AppStack";
import { createSwitchNavigator, createStackNavigator } from "react-navigation";
import LogInScreen from "../screens/LogInScreen";
import AuthLoadingScreen from "../screens/AuthLoadingScreen";

const AuthStack = createStackNavigator({ LogIn: LogInScreen });

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);
