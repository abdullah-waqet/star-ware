import React from "react";
import { connect } from "react-redux";
import { getUser, removeUser } from "../actions";

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  View
} from "react-native";

class LogInScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { credential: { name: "", password: "" } };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.props.loading = false;
  }

  handleSubmit() {
    const { credential } = this.state;

    this.props.onLogin(credential).then(res => {
      if (res.type == "GET_USER_SUCCESS") {
        const { user } = this.props;
        if (user.birth_year == credential.password) {
          this.props.navigation.navigate("AuthLoading");
        } else {
          this.props.reset();
        }
      }
    });
  }

  render() {
    if (this.props.loading) {
      return (
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="white" />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <ScrollView
            style={styles.container}
            contentContainerStyle={styles.contentContainer}
          >
            <View style={styles.welcomeContainer}>
              <Image
                source={require("../assets/images/star-war-logo.png")}
                style={styles.welcomeImage}
              />
            </View>
            <View style={styles.getStartedContainer}>
              <Text style={styles.getStartedText}>
                Welcome to Star Ware App
              </Text>
              <View style={{ margin: 10 }} />
              <TextInput
                style={styles.textInput}
                placeholder="Username"
                onChangeText={text =>
                  this.setState((state, props) => {
                    state.credential.name = text;
                  })
                }
              />
              <TextInput
                style={styles.textInput}
                placeholder="Password"
                secureTextEntry={true}
                onChangeText={text =>
                  this.setState((state, props) => {
                    state.credential.password = text;
                  })
                }
              />

              <TouchableOpacity
                onPress={() => {
                  this.handleSubmit();
                }}
                style={styles.logInButton}
                title="Login"
              >
                <Text style={styles.loginText}>Login</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#333253"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  contentContainer: {
    paddingTop: 30
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(224,91,55, 1)",
    lineHeight: 24,
    textAlign: "center"
  },
  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: "center",
    backgroundColor: "#fbfbfb",
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    textAlign: "center"
  },
  logInButton: {
    marginRight: 40,
    marginLeft: 40,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: "rgba(224,91,55, 1)",
    borderRadius: 10,
    width: 250,
    height: 40
  },
  loginText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 17
  },
  textInput: {
    fontSize: 17,
    width: 250,
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    margin: 7,
    paddingHorizontal: 5,
    textAlign: "center",
    color: "black",
    backgroundColor: "white",
    borderRadius: 10
  }
});

const mapDispatchToProps = dispatch => ({
  onLogin: user => {
    return dispatch(getUser(user.name));
  },
  reset: () => {
    return dispatch(removeUser());
  }
});

const mapStateToProps = state => {
  return { user: state.user, loading: state.loading };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogInScreen);
