import React from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity
} from "react-native";
import { removeUser } from "../actions";
class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: "Profile"
  };

  constructor() {
    super();
  }

  logOut = () => {
    this.props.reset();
    this.props.navigation.navigate("AuthLoading");
  };
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.profileContainer}>
          <Image
            source={require("../assets/images/star-war-logo.png")}
            style={styles.profileImage}
          />
          <View style={{ margin: 20 }} />
          <Text style={styles.userName}>Welcome {this.props.user.name}</Text>

          <TouchableOpacity
            onPress={() => {
              this.logOut();
            }}
            style={styles.logOutButton}
            title="LogOut"
          >
            <Text style={styles.logOutText}>LogOut</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#333253"
  },
  profileContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  profileImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  userName: { fontSize: 20, color: "#E05B37" },
  logOutButton: {
    marginRight: 40,
    marginLeft: 40,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: "rgba(224,91,55, 1)",
    borderRadius: 10,
    width: 250,
    height: 40
  },
  logOutText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 17
  }
});

const mapDispatchToProps = dispatch => ({
  reset: () => {
    return dispatch(removeUser());
  }
});

const mapStateToProps = state => {
  return { user: state.user };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);
