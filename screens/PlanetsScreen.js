import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Text
} from "react-native";
import { connect } from "react-redux";
import { getPlanets } from "../actions";

class PlanetsScreen extends React.Component {
  static navigationOptions = {
    title: "Planets"
  };

  constructor() {
    super();
    this.state = { page: 1 };
  }
  componentDidMount() {
    this.props.getPlanets(this.state.page);
  }

  renderItem = ({ item }) => (
    <TouchableOpacity style={styles.item}>
      <Text style={this.itemTextStyle(item.population)}>{item.name}</Text>
    </TouchableOpacity>
  );

  itemTextStyle = population => {
    if (population <= 1000000) {
      return { fontSize: 10, color: "#EED8A8" };
    } else if (population <= 10000000) {
      return { fontSize: 15, color: "#EED8A8" };
    } else if (population <= 100000000) {
      return { fontSize: 20, color: "#EECB5F" };
    } else if (population <= 1000000000) {
      return { fontSize: 25, color: "#DD852F" };
    } else {
      return { fontSize: 30, color: "#E05B37" };
    }
  };
  onScrollHandler = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.props.getPlanets(this.state.page);
      }
    );
  };

  render() {
    const { planets } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          styles={styles.container}
          data={planets}
          keyExtractor={item => item.key}
          renderItem={this.renderItem}
          onEndThreshold={1}
          onEndReached={this.onScrollHandler}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#333253"
  },
  item: {
    padding: 16
  },
  itemText: {
    color: "#E05B37"
  }
});

const mapDispatchToProps = {
  getPlanets
};

const mapStateToProps = state => {
  let storedPlanets = state.planets.map(planet => ({
    key: planet.name,
    name: planet.name,
    population: planet.population
  }));
  return {
    planets: storedPlanets
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanetsScreen);
