import React from "react";
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View
} from "react-native";
import { connect } from "react-redux";

class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this.props.navigation.navigate(this.props.user.name ? "App" : "Auth");
  }
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return { user: state.user };
};

export default connect(mapStateToProps)(AuthLoadingScreen);
