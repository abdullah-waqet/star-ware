const GET_PLANETS = "GET_PLANETS";
const GET_PLANETS_SUCCESS = "GET_PLANETS_SUCCESS";
const GET_USER = "GET_USER";
const GET_USER_SUCCESS = "GET_USER_SUCCESS";
const REMOVE_USER = "REMOVE_USER";

const initialState = { planets: [], user: {} };

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_PLANETS:
      return { ...state, loading: true };
    case GET_PLANETS_SUCCESS:
      return {
        ...state,
        loading: false,
        planets: [...state.planets, ...action.payload.data.results]
      };
    case GET_USER:
      return { ...state, loading: true };
    case GET_USER_SUCCESS:
      return { ...state, loading: false, user: action.payload.data.results[0] };
    case REMOVE_USER:
      return { ...state, loading: false, user: {}, planets: [] };
    default:
      return state;
  }
}

// import { combineReducers } from 'redux';
// const rootReducer = combineReducers({});
// export default rootReducer;
