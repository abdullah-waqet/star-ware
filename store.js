import { applyMiddleware, createStore } from "redux";
import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import Logger from "redux-logger";
import reducers from "./reducers";

const client = axios.create({
  baseURL: "https://swapi.co/api/",
  responseType: "json"
});

export default createStore(
  reducers,
  applyMiddleware(Logger, axiosMiddleware(client))
);
