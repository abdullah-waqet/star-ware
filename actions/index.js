export function getPlanets(page) {
  return {
    type: "GET_PLANETS",
    payload: {
      request: {
        url: `/planets?page=${page}`
      }
    }
  };
}

export function getUser(name) {
  return {
    type: "GET_USER",
    payload: {
      request: {
        url: `/people?name=${name}`
      }
    }
  };
}

export function removeUser() {
  return {
    type: "REMOVE_USER"
  };
}
